# README - Tupper's self-referential formula #


### What is this repository for? ###

This is a python program designed to both plot [Tupper's self-referential formula](http://en.wikipedia.org/wiki/Tupper%27s_self-referential_formula) and to make the number that is necessary for making a plot.

### How do I get set up? ###

* To plot something from a given number use Tuppers_form.py
* To draw and make a number from that drawing use make_num.py

### Ploting a plot with Tuppers_form.py ###

After running the program you will be prompted for a seed. This is were along the y-axis the plot should start. If you enter 0 then the it will plot the formula it self. If you have a save file that you want to plot you can type "load save" and it will plot your save.

### Making a drawing with make_num.py ###


#### Movment inside of the window ####

To move move the cursor use the arrow keys

#### Drawing ####

If you want to colour or decolour a square press space bar. The colour won't aper before the cursor is moved away from it

#### Saving and gaining the k-value ####

When you are happy with your drawing you can press the enter key to find out where along the y-axis your plot will be. If you would like to save that number for the future you can then press "w" after you have pressed enter. That will save the number in a txt-file called "number.txt". Saving like this won't overwrite any previous values stored in that file, it will simply add it to the end of the file.
If you would like to continue on your drawing you can instead save it in a pickle-file. Too do this you simply press "s" and a file called "save.pickle" will be created if it dosen't exist. This meathod will override any previous data stored in "save.pickle". This method is therefore not recommended for long-term saving.