#! /usr/bin/env python3

import curses
import time
import sys
import pickle

class CordinateSystem:

    def __init__(self, stdscr, magic_number):
        self.stdscr = stdscr
        self.stdscr.clear()
        curses.curs_set(0)

        self.y_dims = (magic_number, magic_number + 17)
        self.x_dims = (0, 106)

        self.graph_win = curses.newwin(19, 108, 0, 0)
        self.graph_win.border()

        self.draw_plot()

    def draw_plot(self):
        for y in range(self.y_dims[0], self.y_dims[1]):
            for x in range(self.x_dims[0], self.x_dims[1]):
                if 1/2 < (y//17//2**(17*int(x) + int(y) % 17)) %2:
                    self.addstr(y - self.y_dims[0], x, '.', curses.A_STANDOUT)
                    self.graph_win.refresh()
                    time.sleep(0.03)
                
        self.wait_for_input()
        curses.endwin()
    
    def wait_for_input(self):
        self.graph_win.getch()
    
    def addstr(self, y, x, str_, color = None):
        x = 106-x
        y += 1
        if color is None:
            self.graph_win.addstr(y, x, str_)
        else:
            self.graph_win.addstr(y, x, str_, color)
    

def load_number(name):
    saved_numbers = {}
    with open('number.txt') as numbers:
        raw_data = numbers.read()
        raw_data = raw_data.split('\n')
        while '' in raw_data:
            raw_data.remove('')
        raw_data = ':'.join(raw_data).split(':')
        for index in range(0, len(raw_data), 2):
            saved_numbers[raw_data[index]] = int(raw_data[index + 1])
    return saved_numbers[name]


def main():
    while True:
        try:
            inp = input('Seed: ')
            if inp.isnumeric():
                inp = int(inp)
            elif inp.lower().startswith('load'):
                if inp == 'load standard':
                    inp = 960939379918958884971672962127852754715004339660129306651505519271702802395266424689642842174350718121267153782770623355993237280874144307891325963941337723487857735749823926629715517173716995165232890538221612403238855866184013235585136048828693337902491454229288667081096184496091705183454067827731551705405381627380967602565625016981482083418783163849115590225610003652351370343874461848378737238198224849863465033159410054974700593138339226497249461751545728366702369745461014655997933798537483143786841806593422227898388722980000748404719
                elif inp == 'load save':
                    inp = save = pickle.load(open('save.pickle', 'rb'))
                else:
                    inp = load_number(inp[5:])
            #curses.wrapper(CordinateSystem, inp)
            CordinateSystem(curses.initscr(), inp)
        except KeyboardInterrupt:
            print()
            sys.exit()
        except EOFError:
            print()
            sys.exit()
        except TypeError:
            print('Seed need to be an integer or start with load')
        except KeyError:
            print("No save named: '%s'" %(inp[5:]))
        except FileNotFoundError:
            print("No save file found")
    

if __name__ == '__main__':
    main()
