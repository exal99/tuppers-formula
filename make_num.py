#! /usr/bin/env python3

import curses
import pickle

class CordinateSystem:
    
    def __init__(self, stdscr, saved_seed = 0):
        self.screen = stdscr
        self.screen.clear()
        curses.noecho()
        curses.curs_set(0)

        self.y_dims = (0, 17)
        self.x_dims = (0, 106)

        self.graph_win = curses.newwin(19, 108, 0, 0)
        self.graph_win.keypad(True)
        self.graph_win.border()
        
        self.text_win = curses.newwin(8, 108, 19, 0)
        self.text_win.border()
        self.text_win.refresh()
        
        self.bit_map = [['0' for x in range(106)] for y in range(17)]
        if saved_seed != 0:
            self.load_save(saved_seed)
        self.draw()

    def draw(self):
        y = 17 #y
        x = 1 #x
        done = False
        key = -1
        draw = False
        while not done:
            try:
                self.graph_win.addch(x, y, '*')
                self.graph_win.refresh()
                
                if key == -1:
                    key = self.graph_win.getch()
                if self.bit_map[y - 1][x - 1] == '0':
                    self.graph_win.addch(x, y, ' ')
                else:
                    self.graph_win.addch(x, y, '.', curses.A_STANDOUT)
                
                self.text_win.clear()
                self.text_win.border()
                self.text_win.refresh()
                
                #quit
                if key == ord('q'):
                    self.text_win.addstr(1,1,'Are you sure you want to quit?')
                    self.text_win.addstr(2,1,'All unsaved data will be lost!')
                    self.text_win.addstr(3,1,'Press ENTER to continue')
                    self.text_win.refresh()
                    key = self.graph_win.getch()
                    if key == curses.KEY_ENTER or key == 10:
                        done = True
                        break
            
                #movment
                if key >= 258 and key <= 261:
                    if key == curses.KEY_DOWN and y != 17:
                        y += 1
                    if key == curses.KEY_UP and y != 1:
                        y -= 1
                    if key == curses.KEY_LEFT and x != 1:
                        x -= 1
                    if key == curses.KEY_RIGHT and x != 106:
                        x += 1
                    key = -1
                
                #painting
                if key == ord(' '):
                    self.bit_map[y-1][x-1] = str((int(self.bit_map[y-1][x-1]) + 1) % 2)
                    if self.bit_map[y - 1][x - 1] == '0':
                        self.graph_win.addch(x, y, ' ')
                    else:
                        self.graph_win.addch(x, y, '.', curses.A_STANDOUT)
                    key = -1
                    
                #finisning
                if key == curses.KEY_ENTER or key == 10:
                    #self.text_win.clear()
                    bin_num = self.make_bin()
                    self.text_win.addstr(1, 1, str(int(bin_num, 2) * 17))
                    self.text_win.refresh()
                    key = self.graph_win.getch()
                    if key == ord('w'):
                        with open('number.txt', 'a') as num_file:
                            num_file.write('\n' + str(int(bin_num, 2) *17))
                        key = -1
                #saving
                if key == ord('s'):
                    num = int(self.make_bin(),2) * 17
                    #self.text_win.addstr(2,1, str(num))
                    pickle.dump(num, open("save.pickle", "wb"))
                    self.text_win.addstr(1,1, 'Save Complet')
                    self.text_win.refresh()
                    key = -1
                
            except KeyboardInterrupt:
                self.text_win.addstr(1,1,'Are you sure you want to quit?')
                self.text_win.addstr(2,1,'All unsaved data will be lost!')
                self.text_win.addstr(3,1,'Press ENTER to continue')
                self.text_win.refresh()
                key = self.graph_win.getch()
                key = self.graph_win.getch() #has to be to getch otherwise key will only be equal to -1
                if key == curses.KEY_ENTER or key == 10:
                    done = True
                else:
                    key = -1
            except EOFError:
                self.text_win.addstr(1,1,'Are you sure you want to quit?')
                self.text_win.addstr(2,1,'All unsaved data will be lost!')
                self.text_win.addstr(3,1,'Press ENTER to continue')
                self.text_win.refresh()
                key = self.graph_win.getch()
                if key == curses.KEY_ENTER or key == 10:
                    done = True
                    #break
        #for returning to the normal terminal            
        curses.nocbreak()
        self.screen.keypad(False)
        curses.echo()
        curses.endwin()
            
        
    def make_bin(self):
        invert_bit_map = [[self.bit_map[row][col] for row in range(17)][::-1] for col in range(106)]
        for index in range(len(invert_bit_map)):
            invert_bit_map[index] = ''.join(invert_bit_map[index])
        return '0b' + ''.join(invert_bit_map)
        
    def load_save(self, seed):
        for y in range(seed, seed + 17):
            for x in range(106):
                if 1/2 < (y//17//2**(17*int(x) + int(y) % 17)) %2:
                    self.graph_win.addstr(y - seed + 1, 106-x, '.', curses.A_STANDOUT)
                    self.bit_map[y- seed][105-x] = '1'
                    self.graph_win.refresh()

                    
def main():
    correct = False
    while not correct:
        try:
            inp = input("Load save? (y/n) ")
            if inp == 'y' or inp == 'yes':
                seed = pickle.load(open('save.pickle', 'rb'))
                correct = True
                CordinateSystem(curses.initscr(), seed)
            elif inp == 'n' or inp == 'no':
                correct = True
                CordinateSystem(curses.initscr())
            else:
                print("Please write 'y', 'yes', 'n' or 'no")
        except FileNotFoundError:
            print("No save file detected")
            print("Continuing without save")
            print("Press ENTER")
            input()
            correct = True
            CordinateSystem(curses.initscr())
        except KeyboardInterrupt:
            print()
            correct = True
        except EOFError:
            print()
            correct = True


if __name__ == '__main__':
    main()
